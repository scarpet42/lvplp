# La Virtualisation Pour Les Piétons

Ce projet est destiné à produire un document LaTeX.

## Le but du projet est de produire un support de présentation

Le cloud est de plus en plus présent tout en étant de moins en moins bien compris.
Il y a de plus en plus de choses qui sont dites et écrites à son propos.
Il devient de plus en plus dur pour les personnes intéressées de savoir qui croire et quoi comprendre.
La virtualisation est au centre du cloud : il est impossible de comprendre le cloud sans comprendre la virtualisation.
Ce qui n'est pas instantané puisqu'il y a deux sortes de virtualisations (les machines virtuelles et la conteneurisation).

Mon but est donc de montrer la virtualisation.
De façon à ce que les gens n'aient pas à me faire confiance mais à voir ce que je présente pour comprendre.
Le but de ce document est donc à la fois un aide mémoire pour m'aider à suivre le fil et un aide mémoire pour les personnes qui auront suivies ma présentation.
Ce document n'est pas destiné à être diffusé.
Il ne se suffit pas en lui-même et sa lecture sans présentation risque d'être confuse.

Je me permets certains raccourcis quand la précision demanderait trop d'explications.
Le but n'est pas d'avoir un lexique précis et complet, mais d'apporter une compréhension des sujets.

## Lecture du document compilé
Le document compilé est disponible ici : [lvplp.pdf](https://scarpet42.gitlab.io/lvplp/lvplp.pdf)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
